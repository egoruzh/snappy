<?php

interface IBootstrap {

    const linuxBin = __DIR__ . '/vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64';
    const winBin = __DIR__ . '\\vendor\\wemersonjanuario\\wkhtmltopdf-windows\\bin\\64bit\\wkhtmltoimage.exe';

    public function installConverters();
}

class ComposerBootstrapConverters implements IBootstrap {

    public function __construct() {
        $json = '{
                "name": "egoru/snappy",
                "type": "library",
                "description": "PHP5 library allowing thumbnail, snapshot or PDF generation from a url or a html page. Wrapper for wkhtmltopdf/wkhtmltoimage.",
                "keywords": ["pdf", "thumbnail", "snapshot", "knplabs", "knp", "wkhtmltopdf"],
                "homepage": "http://github.com/KnpLabs/snappy",
                "version": "1.0.5",
                "license": "MIT",
                "authors": [
                    {
                        "name": "KnpLabs Team",
                        "homepage": "http://knplabs.com"
                    },
                    {
                        "name": "Symfony Community",
                        "homepage": "http://github.com/KnpLabs/snappy/contributors"
                    }
                ],
                "require": {
                    "php": ">=5.6",
                    "symfony/process": "~2.3 || ~3.0 || ~4.0",
                    "psr/log": "^1.0",

                     "knplabs/knp-snappy":"^1.0.0",

                      "wemersonjanuario/wkhtmltopdf-windows":"~0.12",
                      "h4cc/wkhtmltopdf-amd64": "~0.12",
                      "h4cc/wkhtmltoimage-amd64":"~0.12"

                },
                "require-dev": {
                    "phpunit/phpunit": "~4.8.36"
                },
                "suggest": {
                    "h4cc/wkhtmltopdf-amd64": "Provides wkhtmltopdf-amd64 binary for Linux-compatible machines, use version `~0.12` as dependency",
                    "h4cc/wkhtmltopdf-i386": "Provides wkhtmltopdf-i386 binary for Linux-compatible machines, use version `~0.12` as dependency",
                    "h4cc/wkhtmltoimage-amd64": "Provides wkhtmltoimage-amd64 binary for Linux-compatible machines, use version `~0.12` as dependency",
                    "h4cc/wkhtmltoimage-i386": "Provides wkhtmltoimage-i386 binary for Linux-compatible machines, use version `~0.12` as dependency",
                    "wemersonjanuario/wkhtmltopdf-windows": "Provides wkhtmltopdf executable for Windows, use version `~0.12` as dependency"
                },
                
                "extra": {
                    "branch-alias": {
                        "dev-master": "1.0.x-dev"
                    }
                }
            }';

        file_put_contents('composer.json', $json);

        $this->installConverters();
    }

    public function installConverters() {
        system('php composer.phar install');
    }

}

class Env {

    public function __construct() {
        if (file_exists(__DIR__ . '/vendor/autoload.php')) {
            require_once __DIR__ . '/vendor/autoload.php';
        } else {
            Env::installComposer();

            $b = new ComposerBootstrapConverters();
            $b->installConverters();

            exit(0);
        };
    }

    public static function isWindows() {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function installComposer() {
        if (self::isWindows()) {
            $php = 'php';
        } else {
            $php = './php';
        }
        system('wget https://raw.githubusercontent.com/composer/getcomposer.org/1b137f8bf6db3e79a38a5bc45324414a6b1f9df2/web/installer -O - -q | ' . $php . ' -- --quiet');
    }

    public static function getBinaryPath() {
        return self::isWindows() ? IBootstrap::winBin : IBootstrap::linuxBin;
    }

}

new Env();

use Knp\Snappy\Image;

class WebImage extends Image {

    protected $url;
    protected $options;

    function __construct($url_ = 'about:blank') {
        parent::__construct();
        $this->url = $url_ == null ? $_GET['url'] : $url_;

        $this->setBinary(Env::getBinaryPath());
    }

    public function generateFromUrl($output) {

        return $this->generate($this->url, $output, array(), true);
    }

    public function download($file) {

        return $this->getOutput($file);
    }

}

$testObject = new WebImage($_GET['url'] == null ? "http://google.com" : $_GET['url']);


$res = $testObject->generateFromUrl(__DIR__ . "/file.jpg");
echo '<img src="file.jpg"/>';
?>